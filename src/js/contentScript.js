import "../img/icon-appimage-download.svg";
import "../img/icon-appimage-query.svg";
import "../img/icon-appimage-wait.svg";
import "../img/icon-appimage-fail.svg";

'use strict';

const {Gitlab} = require('gitlab');
var $ = require('jquery');


const api = new Gitlab({
    token: 'gh1FnfKxBb-RM75aS5ca'
});

function job_status_to_appimage_icon_suffix(status) {
    switch (status) {
        case 'query' :
            return 'query';
        case 'pending' :
        case 'started' :
            return 'wait';
        case 'success' :
            return 'download';
        default:
            return 'fail';
    }
}

function job_status_to_message(status) {
    switch (status) {
        case 'pending' :
            return 'Job is pending';
        case 'started' :
            return 'Job is started';
        case 'success' :
            return 'Download AppImage';
        case 'failed' :
            return 'Job failed';
        case 'cancelled' :
            return 'Job cancelled';
        default:
            return status;
    }
}

function job_status_to_appimage_icon(status) {
    return chrome.extension.getURL('icon-appimage-' + job_status_to_appimage_icon_suffix(status) + '.svg');
}

function appimage_download_url_by_sha(sha, callback) {
    let inkscape_project_id = "3472737";

    api.Commits.show(inkscape_project_id, sha).then(function (commit) {
        if (commit.last_pipeline == null || commit.last_pipeline.id == null) {
            callback(null, 'Pipeline is not found');
            return;
        }

        api.Jobs.showPipelineJobs(inkscape_project_id, commit.last_pipeline.id).then(function (jobs) {
            var index;
            for (index = 0; index < jobs.length; ++index) {
                var job = jobs[index];
                if (job.name === 'appimage:linux') {
                    if (job.status === 'success') {
                        callback(job.web_url + "/artifacts/raw/" + "Inkscape-" + sha.substr(0, 7) + "-x86_64.AppImage", job.status);
                    } else {
                        callback(null, job.status);
                    }
                    return;
                }
            }
            callback(null, 'AppImage job is not found');
        });
    })
}

function add_appimage_download_links() {
    $("a[href]:not(.artifact-download-link)").each(function (index) {
        let match = this.href.match('^https://gitlab.com/inkscape/inkscape/-/commit/([a-z0-9]{40})$');
        if (match) {
            let sha = match[1];
            if (!$(this).next().hasClass("artifact-download-link")) {

                let download_link = $("<a title='Querying artifacts...' class='artifact-download-link'><img src='" + job_status_to_appimage_icon("query") + "'></a>");
                download_link.insertAfter(this);
                download_link = $(this).next();

                appimage_download_url_by_sha(sha, function (url, status) {
                    download_link.children("img").attr("src", job_status_to_appimage_icon(status));
                    download_link.attr("title", job_status_to_message(status));
                    download_link.attr("href", url);
                });
            }
        }
    });
}

$(document).ready(
    function () {
        add_appimage_download_links();
        $(document).dblclick(function () {
            add_appimage_download_links();
        });
    }
);